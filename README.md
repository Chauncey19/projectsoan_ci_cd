Symfony Demo + Api Platform
========================


Requirements 
------------

  * For this information, the documentation has been tested with environnement Linux (Debian ou Ubuntu)
  * PHP 7.3 or higher;
  * PDO-SQLite PHP extension enabled (php.ini);
  * and the [usual Symfony application requirements][2].
  * Composer 2.0

Installation
------------

[Download Symfony][4] to install the `symfony` binary on your computer and run
this command:

```bash
$ symfony new --demo my_project
```

Alternatively, you can use Composer:

```bash
$ composer create-project symfony/symfony-demo my_project
```

Download Composer to install Composer ou upgrade :
https://doc.ubuntu-fr.org/composer

Usage
-----

There's no need to configure anything to run the application and install package of this project. If you have
[installed Symfony][4] binary, run this command:

```bash
$ cd my_project/
$ composer install
$ symfony serve
```

Then access the application in your browser at the given URL (<https://localhost:8000> by default).

Then access the API platform in your browser at the given URL (<https://localhost:8000/api>)



Doc
-----
In this project, i used this documentation :

* api platform : https://api-platform.com/docs/core/getting-started/
* gitlab-ci : https://docs.gitlab.com/ee/ci/quick_start/
* gitlab runner: https://docs.gitlab.com/runner/install/linux-manually.html

-----

